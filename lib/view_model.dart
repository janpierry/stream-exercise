import 'dart:async';

import 'package:mvvm/repository/repository_memory.dart';
import 'package:mvvm/task_model.dart';

class TaskViewModel {
  final _repository = RepositoryMemory();
  final _streamController = StreamController.broadcast();

  List<TaskModel> get tasks => _repository.list();
  TaskModel? taskSelected;

  Stream get stream => _streamController.stream;

  void dispose() {
    _streamController.close();
  }

  void saveTask(String description) {
    if (taskSelected == null) {
      final id = DateTime.now().microsecondsSinceEpoch.toString();
      _repository.create(TaskModel(description: description, id: id));
    } else {
      _repository.update(taskSelected!);
    }

    _streamController.add(0);
  }

  void removeTask(TaskModel task) {
    if (task == taskSelected) {
      taskSelected = null;
    }
    _repository.remove(task);
    _streamController.add(0);
  }

  void setTaskSelected(TaskModel task) {
    taskSelected = task;
    _streamController.add(0);
  }
}
